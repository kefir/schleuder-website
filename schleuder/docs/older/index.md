---
title: Older documentation
---

Here are links to the documentation for older versions of Schleuder:

* [3.1](3.1/)
* [3.0](3.0/)
* [2.x](https://schleuder2.nadir.org/documentation.html)

