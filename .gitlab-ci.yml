---
image: debian:unstable

stages:
  - static
  - build
  - deploy

cache:
  paths:
    - vendor

# Jobs that start with a period are disabled
# This is just a template, to be used further below in the individual job definitions
.setup_apt: &setup_apt
  before_script:
    # Export APT env vars to cache packages archives and lists based on the current working directory
    - export APT_DIR=$CI_PROJECT_DIR/vendor/apt && export APT_ARCHIVES_DIR=$APT_DIR/archives && export APT_LISTS_DIR=$APT_DIR/lists
    # Configure APT: Only install necessary packages, set cache location
    - printf
      "apt::install-recommends 0;\n
      apt::install-suggests 0;\n
      dir::cache::archives ${APT_ARCHIVES_DIR};\n
      dir::state::lists ${APT_LISTS_DIR};\n"
      >> /etc/apt/apt.conf.d/99custom
    # Ensure the custom APT directory does exist
    - mkdir -p {${APT_ARCHIVES_DIR},${APT_LISTS_DIR}}/partial
    - apt-get update -qq
    # To keep things DRY, use an env var to handle packages to be installed via APT
    - apt-get install -qq -y $APT_INSTALL_PACKAGES

codespell:
  variables:
    APT_INSTALL_PACKAGES: codespell
  <<: *setup_apt
  script:
    # Run codespell to check for spelling errors, using a config with ignored words,
    # ignoring warnings about binary files and to check file names as well.
    # Also, exclude the vendor dir, which leads to false positives.
    - codespell -q 2 -f -I utils/ci/codespell/ignored_words.txt -S vendor
  stage: static

build:
  variables:
    APT_INSTALL_PACKAGES: jekyll
  <<: *setup_apt
  script:
    - jekyll build
    - tar cfz site.tar.gz _site
  stage: build
  artifacts:
    paths:
      - site.tar.gz
    expire_in: 2 weeks

deploy:
  variables:
    APT_INSTALL_PACKAGES: lftp openssh-client
  <<: *setup_apt
  script:
    # Disable bash history to prevent secret variables to be recorded and saved
    - unset HISTFILE
    # Start SSH agent
    - eval $(ssh-agent -s)
    # Add the SSH key stored in the SSH_DEPLOY_KEY variable to the agent store
    # We're using 'tr' to fix line endings which makes ed25519 keys work without 
    # extra base64 encoding
    - echo "$DEPLOY_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - printf "
      |1|ZfxGVbfwfCHlaURlet/V6y+2gjg=|/X7OweXQUnXZnGSKkvF/IpVz4n4= ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPJx38PfGvaHtkSsHptiHoIQxlI3Yf0cskPNTwAQnY14\n
      |1|8YPsezXF2SYQ7rq9U5TbDnMsVjo=|SJOodZB+8j+dO+l6YTdZ7+44XLw= ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPJx38PfGvaHtkSsHptiHoIQxlI3Yf0cskPNTwAQnY14
      " > ~/.ssh/known_hosts
    - tar fxv site.tar.gz
    # In case we're not dealing with the master branch, create the review dir on the webserver.
    # We need to catch possible errors, as lftp seems buggy: It fails if the dir already exists,
    # even if called with the -f option, which should ignore errors (like this one).
    # lftp needs a "dummy" password, even if key-based authentication is used.
    - if ! [ "$CI_COMMIT_REF_SLUG" == "master" ]; then DEPLOY_SLUG="www/review/$CI_COMMIT_REF_SLUG" &&
      lftp -e "mkdir -fp $DEPLOY_SLUG; quit" -u $DEPLOY_USER,dummy sftp://$DEPLOY_HOST || /bin/true;
      else DEPLOY_SLUG=www;
      fi
    - lftp -e "mirror -eRv -x ^download/ -x ^review/ _site $DEPLOY_SLUG; quit;" -u $DEPLOY_USER,dummy sftp://$DEPLOY_HOST
    # TODO: Implement clean up: Remove the review dir once the merge to master happened.
  stage: deploy
  # It's currently not possible to use env vars which are / were set in the script: part above.
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://schleuder.org/review/$CI_COMMIT_REF_SLUG
  only:
    # This job relies on secret env vars, which are only available in our repo.
    - branches@schleuder/schleuder-website
