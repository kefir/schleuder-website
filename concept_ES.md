# Concepto

Este documento hace referencia a la versión 3.2 de Schleuder. Para leer sobre versiones anteriores de Schleuder, véase [esta documentación](https://schleuder.org/schleuder/docs/older/).
 
Schleuder es un gestor de listas con PGP habilitado y la capacidad de reenviar correo. Este documento intenta explicar qué significa esto.


# Comunicación colectiva

Schleuder permite a lxs suscriptorxs comunicar entre sí mediante cifrado y con pseudónimos. Se encarga de cifrar y decifrar los correos, administrar los encabezados y formatos de la comunicación de la lista, etc.

También recibe y envía correos a personas que no están suscritas a la lista: una funcionalidad muy útil para la comunicación colectiva. Schleuder toma el correo entrante de estas personas no suscriptoras y la cifra y distribuye a lxs integrantes de la lista. Estas personas pueden responder entre si el correo y enviar una respuesta hacia la persona no suscriptora de la lista que inició la comunicación.

La persona "de afuera de la lista" sólo ve la dirección de la lista (y su llave pública asociada); no ve las direcciones y llaves de lxs integrantes de la lista.

Aquí puedes ver una imagen simple de un mensaje enviado a una persona que no forma de la lista de correos ("Zacharias"). Ilustra cómo Alice, Bob, Calire y David son invisibles para Zacharias. Haz clic sobre la imagen para verla en grande.

![Esquema de Schleuder](schleuder-schema-resend-small.png "Esquema de Schleuder")


## Un bot que ayuda

Además, Schleuder puede enviar su propia llave pública como respuesta de una petición via correo. Cualquier presona que envía un mensaje a listname-sendkey@hostname recibirá de vuelta la llave pública de la lista.

Schleuder también puede recibir comandos de administración por correo. Por ejemplo, agregar la llave pública de la persona de afuera de la lista que escribió a la lista para poder responderla de manera cifrada.

# Detalles técnicos

## Una "persona intermediarix" deseable

Básicamente, Schleuder es un [intermediarix](https://es.wikipedia.org/wiki/Ataque_de_intermediario) "deseable".

Cada lista tiene su par de llaves. Schleuder de-cifra todos los correos que entran y verifica la firma con el llavero de esa lista (que tiene todas las llaves públicas de las personas integrantes de la lista). Schleuder limpia los datos relacionado con el mensaje y envía una copia a cada suscriptorx, cifrándola con la llave pública correspondiente.

Schleuder inserta algunas líneas de metadatos en la parte superior del correo: una copia configurable de una parte de los encabezados originales del correo y el resultado del cifrado y verificación del correo entrante.

Aquí va un ejemplo:

From: Bob <bob@example.net>
To: team@schleuder.org
Date: Tue, 6 Apr 2010 17:28:46 +0200
Enc: encrypted
Sig: Good signature from 12345678DEADBEEF Bob <bob@example.net>

"Enc: encrypted" significa que el correo está cifrado. "Sig: Good signature..." significa que fue firmado correctamente.

El abordaje de Schleuder implica que tienes que confiar en las personas que están administrando Schleuder en el servidor: podrían almacenar y decifrar todos los correos que pasa por la lista si quisieran.


## Funcionamiento interno

Schleuder se comporta como un filtro de correos.

Si la operación se realiza extitosamente, Schleuder cierra la conexión inicial con el servidor de correo después de haber enviado todos los correos de salida.

En caso de un error, el servidor de correo rebota el correo a la persona emisora, incluyendo el mensaje de error que genera Schleuder.

Los llaveros de cada lista son llaveros PGP estándar y se almacenan en el servidor en un árbol de directorios del tipo "$lists_dir/$hostname/$listname/". Donde "$lists_dir" es una variable que lee la [configuración de Schleuder](https://schleuder.org/schleuder/docs/concept.html#configuration). 

Los llaveros pueden ser utilizados manualmente con gpg2. Ten cuidado con los permisos de los archivos si tocas esta configuración.

En el directorio de la lista también generalmente hay un "log" (historial), al menos que sea muy grande o no haya ocurrido un error aún. También se guarda un historial en 'syslog'. Los detalles de cómo guardar historiales dependerá del sistema operativo y administración del servidor. 

Todos los demás datos relacionados con las listas Schleuder se almacena en la base de datos SQL. La mayor parte de los datos no está [serializada](https://es.wikipedia.org/wiki/Serializaci%C3%B3n). Sólo algunos valores están codificados en JSON. 



Para sugerir cambios a este documento, [contacta con nosotrxs](https://schleuder.org/contact.html). También puedes ver [el historial de cambios](https://0xacab.org/schleuder/schleuder-website/commits/master).


