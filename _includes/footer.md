### Links

To **participate** in the development use the [issue tracker](https://0xacab.org/schleuder/{{ include.project }}/issues). Please take note of our [Code of Conduct](CODE_OF_CONDUCT.html).

To **contact** us read [contact](contact.html).

To **be notified** of news about Schleuder subscribe to [schleuder-announce](https://lists.nadir.org/mailman/listinfo/schleuder-announce).

{% include feedback.md %}
